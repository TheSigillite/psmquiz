import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Button, TouchableOpacity, FlatList, RefreshControl} from 'react-native';
type Props = {};

function Item({nick,score,total,type,date}){
    return(
        <View style={styles.itemOpacity}>
            <Text style={styles.itemOpacityTitle}>{nick}{' '}{score}{'/'}{total}</Text>
            <Text style={styles.itemOpacityDesc}>{type}{' '}{date}</Text>
        </View>
    )
}


export default class ResultsScreen extends Component<Props>{

    constructor(Props) {
        super(Props) 
        this.exampleSet = this.getData()
        this.state = {
            refreshing: false,
            dataSource: this.exampleSet
        }
    }

    getData = () =>{
        return fetch('http://tgryl.pl/quiz/results').then((response) => response.json()).then(
            (responseJson) => {this.exampleSet = responseJson
                this.forceUpdate()
                return responseJson}
        ).catch(error => console.error(error))
    }

  

	render(){
        
		return(
            <View style={styles.container}>
                <FlatList
                data={this.exampleSet}
                //renderItem={({item}) =>}
                renderItem={({item}) => <Item 
                nick={item.nick}
                score={item.score}
                total={item.total}
                type={item.type}
                date={item.date}/>}
                refreshControl={
                    <RefreshControl
                    refreshing={this.state.refreshing}
                    onRefresh={this.getData.bind(this)}
                    />
                }
                />
            </View>
    )
    }
}


const styles = StyleSheet.create({
  container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'stretch',
        backgroundColor: 'grey',
  },
  item: {
      flex: 8,
      flexDirection: 'column',
      justifyContent: 'space-around',
      alignItems: 'flex-start',
      backgroundColor: 'red'
  },
  itemOpacity: {
      backgroundColor: '#5E5E5E',
      alignItems: 'center',
      justifyContent: 'space-around',
      alignSelf: 'stretch',
      flexGrow: 1
  },
  itemOpacityTitle: {
      fontSize: 30,
      color: 'white'
  },
  itemOpacityTags: {
      fontSize: 8,
      color: 'blue',
  },
  itemOpacityDesc: {
      fontSize: 20,
      color: 'white'
  }
});