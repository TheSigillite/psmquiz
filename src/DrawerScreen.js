import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Button, TouchableOpacity} from 'react-native';
import {Navigation} from 'react-native-navigation';

export default class Home extends Component<Props>{

    goToScreen = (screenName) => {
        Navigation.push('AppStack',{
            component: {
                name: screenName
            }
        })
    }

    render(){
        return(
            <View style={styles.container}>
                <View style={styles.drawerItemsStyle}>
                    <TouchableOpacity style={styles.btn} onPress={() => this.goToScreen('ResultsScreen')}>
                        <Text style={styles.buttonTex}>Results</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.btn} onPress={() => this.goToScreen('Home')}>
                        <Text style={styles.buttonTex}>Home</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    btn: {
        flex: 2,
        alignItems: 'center',
        alignSelf: "stretch",
        justifyContent: 'center',
        backgroundColor: 'grey'
    },
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    drawerItemsStyle: {
        flexDirection: 'column',
        flex: 3,
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    buttonTex: {
        fontSize: 30,
        color: 'white'
    }
})