import { Navigation } from 'react-native-navigation';

export function registerScreens() {
    Navigation.registerComponent('Home', () => require('./Home').default)
    Navigation.registerComponent('ResultsScreen', () => require('./ResultsScreen').default)
    Navigation.registerComponent('TestsScreen', () => require('./TestsScreen').default)
    Navigation.registerComponent('DrawerScreen', () => require('./DrawerScreen').default)
    Navigation.registerComponent('tosScreen', () => require('./tosScreen').default)
}