import {Navigation} from 'react-native-navigation';

export const gotoHome = () => Navigation.setRoot({
    root: {
        stack: {
            id: 'App',
            children: [
                {
                    component: {
                        name: 'Home'
                    }
                }
            ]
        }
    }
});

export const gotoResults = () => Navigation.setRoot({
    
})