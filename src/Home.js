import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Button, TouchableOpacity} from 'react-native';
import {Navigation} from 'react-native-navigation';
import {ListItem} from 'react-native-elements';

type Props = {};
export default class Home extends Component<Props>{
    
    constructor() {
        super();
        allquizesurl = 'http://tgryl.pl/quiz/tests';
        singlequizurl = 'http://tgryl.pl/quiz/test/';
        availablequizes= this.getquizes()
    }

    goToResuslts = () => {
        Navigation.push(this.props.componentId,{
            component: {
                name: 'ResultsScreen'
            }
        });
    }

    goToTests = () => {
        Navigation.push('AppStack',{
            component: {
                name: 'TestsScreen'
            }
        });
    }

    goToTestsWithProps = (question, quid, cath) => {
        Navigation.push('AppStack',{
            component: {
                name: 'TestsScreen',
                passProps: {
                    toSolve: question,
                    quizid: quid,
                    cathegory: cath
                }
            }
        })
    }


    getquizes = () => {
        return fetch('http://tgryl.pl/quiz/tests').then((response) => response.json()).then(
            (responseJson) => { this.availablequizes = responseJson 
            this.forceUpdate()
            return responseJson}
        )
    }

    getQuizById(idq){
        return fetch('http://tgryl.pl/quiz/test/'+idq).then((response) => response.json()).then(
            (responseJson) => {this.goToTestsWithProps(responseJson.tasks,responseJson.id,responseJson.tags[0])}
        )
    }
    quizTilesBuilder(current){
        console.log(current.id)
        return(
            <TouchableOpacity style={styles.itemOpacity} key={current.id} onPress={() => this.getQuizById(current.id)}>
                <Text style={styles.itemOpacityTitle}>{current.name}</Text>
                <Text style={styles.itemOpacityDesc}>{current.description}</Text>
                <Text style={styles.itemOpacityTags}>{current.tags}</Text>
            </TouchableOpacity>
        )
    }
	render(){
        if(this.availablequizes==undefined){
		return(
			<View style={styles.container}>
				<View style={styles.quizlist}>
                    <TouchableOpacity style={styles.itemOpacity} onPress={() => this.goToTests()}>
                        <Text style={styles.itemOpacityTitle}>Tests</Text>
                    </TouchableOpacity>
                    
                </View>
                <View style={styles.navbox}>
                    <TouchableOpacity style={styles.bottonNavButton} onPress={() => this.goToResuslts()}>
                        <Text style={styles.bottomNavText}>See The Results!</Text>
                    </TouchableOpacity>
                </View>
			</View>
    )}
    else{
        return(
			<View style={styles.container}>
				<View style={styles.quizlist}>
                    <TouchableOpacity style={styles.itemOpacity} onPress={() => this.goToTests()}>
                        <Text style={styles.itemOpacityTitle}>Tests</Text>
                    </TouchableOpacity>
                    {this.availablequizes.map(quiz => this.quizTilesBuilder(quiz))}
                </View>
                <View style={styles.navbox}>
                    <TouchableOpacity style={styles.bottonNavButton} onPress={() => this.goToResuslts()}>
                        <Text style={styles.bottomNavText}>See The Results!</Text>
                    </TouchableOpacity>
                </View>
			</View>
        )}
    }
}

const styles = StyleSheet.create({
  container: {
  flex: 1,
  justifyContent: 'center',
  alignItems: 'center',
  backgroundColor: '#F5FCFF',
  },
  quizlist: {
  flex: 5,
  justifyContent: 'flex-start',
  alignItems: 'stretch',
  backgroundColor: 'red',
  alignSelf: 'stretch'
  },
  navbox: {
    flex: 1,
    justifyContent: 'space-around',
    alignItems: 'stretch',
    backgroundColor: 'black',
    alignSelf: 'stretch'
  },
  bottonNavButton: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#5E5E5E'
  },
  bottomNavText: {
      fontSize: 22,
      color: 'white'
  },
  itemOpacity: {
    backgroundColor: '#5E5E5E',
    alignItems: 'flex-start',
    justifyContent: 'space-around'
},
itemOpacityTitle: {
    fontSize: 20,
    color: 'white',
    fontFamily: 'Ubuntu'
},
itemOpacityTags: {
    fontSize: 8,
    color: 'blue',
},
itemOpacityDesc: {
    fontSize: 10,
    color: 'white',
    fontFamily: 'Biryani'
}
});