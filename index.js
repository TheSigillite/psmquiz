/**
 * @format
 */
import { Navigation } from 'react-native-navigation'
import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import { registerScreens } from './src/screens'

//AppRegistry.registerComponent(appName, () => App);
Navigation.registerComponent('navigation.playground.WelcomeScreen', () => App);
registerScreens();
Navigation.events().registerAppLaunchedListener(() => {
    Navigation.setRoot({
        root: {
            component: {
                name: 'tosScreen'
            }
        }
    });
});